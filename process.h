typedef struct {
	int Pid;
	int Ppid;
	char Cmd[250];
	struct Process** Children;
	int ChildrenCount;
} Process;

Process * Process_new(const int pid, const int ppid, const char cmd[]);

void Print(Process* process, const int subIndex);

void ProcessChild(Process* process, Process* child);

void Build(Process* processes[], Process* tree[], const int maxsize);

void PrintTree(Process* processes[], const int maxLength);

int SplitString(char* str, char* tokens[], const int maxTokens);

Process* CreateProcess(char buffer[]);


