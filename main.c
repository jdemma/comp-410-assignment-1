#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "process.h"

int main ()
{	
	const int BUFFER_MAX = 5000;
	char buffer[BUFFER_MAX];
	char * output;
	Process* processes[500];
	
	FILE *stream;
	stream = popen("ps -aef", "r");
	int index = 0;
	if(stream) 
	{
		while (fgets(buffer, sizeof(buffer), stream) != NULL)
		{			
			processes[index] = CreateProcess(buffer);		
			index ++;		
		}
	}
	Process* tree[index];
	BuildTree(processes, tree, index);	
	PrintTree(tree, index);

	pclose(stream);
}




